"""sitio URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from apps.inicio import views as vista
from apps.noticias.views import vista_noticia, vista_todas
from apps.suscribir import views as inscribir
from apps.multimedia import views as multimedia
from apps.encuesta import views as encuesta

urlpatterns = [
    url(r'^adminunivrso/', admin.site.urls),
    url(r'^api/', include('apps.api.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^404/$', vista.handler404, name='handler404'),
    url(r'^500/$', vista.handler500, name='handler500'),
    url(r'^noticia/(\d+)/$', vista_noticia, name = 'noticia'),
    url(r'^noticias/$', vista_todas, name = 'noticias'),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^inscribir/', inscribir.inscripcion_congreso, name='inscribir'),
    url(r'^suscribir/', inscribir.suscribir_correos, name='suscribir'),
    # url(r'^congreso/', vista.vista_congreso, name='congreso'),
    url(r'^multimedia/', multimedia.vista_video, name='multimedia'),
    url(r'^encuesta/', encuesta.vista_encuesta, name='encuesta'),
    url(r'^$', vista.vista_inicio, name='index'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
