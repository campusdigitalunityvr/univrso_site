

$(document).ready(function(){
  // Muestra modal al cargar la página
  //$('#inicioModal').modal('show');

  var RegionesYcomunas = {
  	"regiones": [{
  			"NombreRegion": "Región de Arica y Parinacota",
  			"comunas": ["Arica", "Camarones", "Putre", "General Lagos"]
  	},
  		{
  			"NombreRegion": "Región de Tarapacá",
  			"comunas": ["Iquique", "Alto Hospicio", "Pozo Almonte", "Camiña", "Colchane", "Huara", "Pica"]
  	},
  		{
  			"NombreRegion": "Región de Antofagasta",
  			"comunas": ["Antofagasta", "Mejillones", "Sierra Gorda", "Taltal", "Calama", "Ollagüe", "San Pedro de Atacama", "Tocopilla", "María Elena"]
  	},
  		{
  			"NombreRegion": "Región de Atacama",
  			"comunas": ["Copiapó", "Caldera", "Tierra Amarilla", "Chañaral", "Diego de Almagro", "Vallenar", "Alto del Carmen", "Freirina", "Huasco"]
  	},
  		{
  			"NombreRegion": "Región de Coquimbo",
  			"comunas": ["La Serena", "Coquimbo", "Andacollo", "La Higuera", "Paihuano", "Vicuña", "Illapel", "Canela", "Los Vilos", "Salamanca", "Ovalle", "Combarbalá", "Monte Patria", "Punitaqui", "Río Hurtado"]
  	},
  		{
  			"NombreRegion": "Región de Valparaíso",
  			"comunas": ["Valparaíso", "Casablanca", "Concón", "Juan Fernández", "Puchuncaví", "Quintero", "Viña del Mar", "Isla de Pascua", "Los Andes", "Calle Larga", "Rinconada", "San Esteban", "La Ligua", "Cabildo", "Papudo", "Petorca", "Zapallar", "Quillota", "Calera", "Hijuelas", "La Cruz", "Nogales", "San Antonio", "Algarrobo", "Cartagena", "El Quisco", "El Tabo", "Santo Domingo", "San Felipe", "Catemu", "Llaillay", "Panquehue", "Putaendo", "Santa María", "Quilpué", "Limache", "Olmué", "Villa Alemana"]
  	},
  		{
  			"NombreRegion": "Región del Libertador Gral. Bernardo O’Higgins",
  			"comunas": ["Rancagua", "Codegua", "Coinco", "Coltauco", "Doñihue", "Graneros", "Las Cabras", "Machalí", "Malloa", "Mostazal", "Olivar", "Peumo", "Pichidegua", "Quinta de Tilcoco", "Rengo", "Requínoa", "San Vicente", "Pichilemu", "La Estrella", "Litueche", "Marchihue", "Navidad", "Paredones", "San Fernando", "Chépica", "Chimbarongo", "Lolol", "Nancagua", "Palmilla", "Peralillo", "Placilla", "Pumanque", "Santa Cruz"]
  	},
  		{
  			"NombreRegion": "Región del Maule",
  			"comunas": ["Talca", "ConsVtución", "Curepto", "Empedrado", "Maule", "Pelarco", "Pencahue", "Río Claro", "San Clemente", "San Rafael", "Cauquenes", "Chanco", "Pelluhue", "Curicó", "Hualañé", "Licantén", "Molina", "Rauco", "Romeral", "Sagrada Familia", "Teno", "Vichuquén", "Linares", "Colbún", "Longaví", "Parral", "ReVro", "San Javier", "Villa Alegre", "Yerbas Buenas"]
  	},
  		{
  			"NombreRegion": "Región del Biobío",
  			"comunas": ["Concepción", "Coronel", "Chiguayante", "Florida", "Hualqui", "Lota", "Penco", "San Pedro de la Paz", "Santa Juana", "Talcahuano", "Tomé", "Hualpén", "Lebu", "Arauco", "Cañete", "Contulmo", "Curanilahue", "Los Álamos", "Tirúa", "Los Ángeles", "Antuco", "Cabrero", "Laja", "Mulchén", "Nacimiento", "Negrete", "Quilaco", "Quilleco", "San Rosendo", "Santa Bárbara", "Tucapel", "Yumbel", "Alto Biobío", "Chillán", "Bulnes", "Cobquecura", "Coelemu", "Coihueco", "Chillán Viejo", "El Carmen", "Ninhue", "Ñiquén", "Pemuco", "Pinto", "Portezuelo", "Quillón", "Quirihue", "Ránquil", "San Carlos", "San Fabián", "San Ignacio", "San Nicolás", "Treguaco", "Yungay"]
  	},
  		{
  			"NombreRegion": "Región de la Araucanía",
  			"comunas": ["Temuco", "Carahue", "Cunco", "Curarrehue", "Freire", "Galvarino", "Gorbea", "Lautaro", "Loncoche", "Melipeuco", "Nueva Imperial", "Padre las Casas", "Perquenco", "Pitrufquén", "Pucón", "Saavedra", "Teodoro Schmidt", "Toltén", "Vilcún", "Villarrica", "Cholchol", "Angol", "Collipulli", "Curacautín", "Ercilla", "Lonquimay", "Los Sauces", "Lumaco", "Purén", "Renaico", "Traiguén", "Victoria", ]
  	},
  		{
  			"NombreRegion": "Región de Los Ríos",
  			"comunas": ["Valdivia", "Corral", "Lanco", "Los Lagos", "Máfil", "Mariquina", "Paillaco", "Panguipulli", "La Unión", "Futrono", "Lago Ranco", "Río Bueno"]
  	},
  		{
  			"NombreRegion": "Región de Los Lagos",
  			"comunas": ["Puerto Montt", "Calbuco", "Cochamó", "Fresia", "FruVllar", "Los Muermos", "Llanquihue", "Maullín", "Puerto Varas", "Castro", "Ancud", "Chonchi", "Curaco de Vélez", "Dalcahue", "Puqueldón", "Queilén", "Quellón", "Quemchi", "Quinchao", "Osorno", "Puerto Octay", "Purranque", "Puyehue", "Río Negro", "San Juan de la Costa", "San Pablo", "Chaitén", "Futaleufú", "Hualaihué", "Palena"]
  	},
  		{
  			"NombreRegion": "Región Aisén del Gral. Carlos Ibáñez del Campo",
  			"comunas": ["Coihaique", "Lago Verde", "Aisén", "Cisnes", "Guaitecas", "Cochrane", "O’Higgins", "Tortel", "Chile Chico", "Río Ibáñez"]
  	},
  		{
  			"NombreRegion": "Región de Magallanes y de la Antártica Chilena",
  			"comunas": ["Punta Arenas", "Laguna Blanca", "Río Verde", "San Gregorio", "Cabo de Hornos (Ex Navarino)", "Antártica", "Porvenir", "Primavera", "Timaukel", "Natales", "Torres del Paine"]
  	},
  		{
  			"NombreRegion": "Región Metropolitana de Santiago",
  			"comunas": ["Cerrillos", "Cerro Navia", "Conchalí", "El Bosque", "Estación Central", "Huechuraba", "Independencia", "La Cisterna", "La Florida", "La Granja", "La Pintana", "La Reina", "Las Condes", "Lo Barnechea", "Lo Espejo", "Lo Prado", "Macul", "Maipú", "Ñuñoa", "Pedro Aguirre Cerda", "Peñalolén", "Providencia", "Pudahuel", "Quilicura", "Quinta Normal", "Recoleta", "Renca", "San Joaquín", "San Miguel", "San Ramón", "Vitacura", "Puente Alto", "Pirque", "San José de Maipo", "Colina", "Lampa", "TilVl", "San Bernardo", "Buin", "Calera de Tango", "Paine", "Melipilla", "Alhué", "Curacaví", "María Pinto", "San Pedro", "Talagante", "El Monte", "Isla de Maipo", "Padre Hurtado", "Peñaflor"]
  	}]
  }

  // Suscribir a congreso
  $("#form_inscribir").submit(function(event) {

      $("#enviar_congreso").html("Guardando");
      $('#enviar_congreso').prop('disabled', true);

      $.ajax({
        type: "POST",
        url: '/inscribir/',
        data: {
            nombre: $('#id_nombre').val(),
            institucion: $('#id_institucion').val(),
            correo: $('#id_correo').val(),
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        },
        success: function(data) {
          if(data.estado == "ok_congreso_email"){
            $("#mensajeRespuesta").modal("show");
            $("#congresoModal").modal("hide");
            $("#form_inscribir")[0].reset();
            console.log("Se ha registrado al usuario a la base de datos y se ha enviado el correo");
          }
          else if(data.estado == "ok_congreso"){
            $("#mensajeRespuesta").modal("show");
            $("#congresoModal").modal("hide");
            $("#form_inscribir")[0].reset();
            console.log("Se ha registrado usuario en la base de datos");
          }
          else if(data.estado == "nok_congreso"){
            console.log("No se ha podido registrar usuario al congreso");
          }
          else {
            console.log("error desde servidor");
          }
        }
      }).done(function() {
        console.log("Fin congreso");
      });
      return false;
    });

  // Suscribirse al listado de correos
  $("#form_correos").submit(function(event) {
      $.ajax({
        type: "POST",
        url: '/suscribir/',
        data: {
            email: $('#id_email').val(),
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        },
        success: function(data) {
          if(data.estado == "ok_lista_correo"){
            $("#respuestaListaCorreo").modal("show");
            $("#suscribirModal").modal("hide");
            $("#form_correos")[0].reset();
            console.log("Se ha registrado usuario a listado de correos");
          }
          else if(data.estado == "nok_lista_correo"){
            console.log("No se ha podido registrar usuario al listado de correos");
          }
          else {
            console.log("error desde servidor");
          }
        }
      }).done(function() {
        console.log("Fin Listado de correos");
      });
      return false;
    });

  /// Funciones asociadas al formulario de Encuesta Mooc 1
  // Habilita otro medios con los cuales se informo
  $("#id_informo").change(function() {
    var checked = $('input[name=informo]:checked').val();
    if (checked == 'otro') {
      $('#otro_medio').attr('readonly', false);
    }
    else {
      $('#otro_medio').attr('readonly', true);
    }
  });


  // Funciones asociadas al cambio de Región y Comunas en Formulario Encuestas Mooc
  var iRegion = 0;
  var htmlRegion = '<option value="sin-region">Seleccione región</option><option value="sin-region">--</option>';
  var htmlComunas = '<option value="sin-region">Seleccione comuna</option><option value="sin-region">--</option>';

  jQuery.each(RegionesYcomunas.regiones, function () {
    htmlRegion = htmlRegion + '<option value="' + RegionesYcomunas.regiones[iRegion].NombreRegion + '">' + RegionesYcomunas.regiones[iRegion].NombreRegion + '</option>';
    iRegion++;
  });

  jQuery('#regiones').html(htmlRegion);
  jQuery('#comunas').html(htmlComunas);

  jQuery('#regiones').change(function () {
    var iRegiones = 0;
    var valorRegion = jQuery(this).val();
    var htmlComuna = '<option value="sin-comuna">Seleccione comuna</option><option value="sin-comuna">--</option>';
    jQuery.each(RegionesYcomunas.regiones, function () {
      if (RegionesYcomunas.regiones[iRegiones].NombreRegion == valorRegion) {
        var iComunas = 0;
        jQuery.each(RegionesYcomunas.regiones[iRegiones].comunas, function () {
          htmlComuna = htmlComuna + '<option value="' + RegionesYcomunas.regiones[iRegiones].comunas[iComunas] + '">' + RegionesYcomunas.regiones[iRegiones].comunas[iComunas] + '</option>';
          iComunas++;
        });
      }
      iRegiones++;
    });
    jQuery('#comunas').html(htmlComuna);
  });
  jQuery('#comunas').change(function () {
    if (jQuery(this).val() == 'sin-region') {
      alert('selecciones Región');
    } else if (jQuery(this).val() == 'sin-comuna') {
      alert('selecciones Comuna');
    }
  });
  jQuery('#regiones').change(function () {
    if (jQuery(this).val() == 'sin-region') {
      alert('selecciones Región');
    }
  });

});
