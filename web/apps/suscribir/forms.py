#!/usr/bin/python
# -*- coding: utf-8 -*-
from django import forms
from .models import Suscribir, Congreso

class FormularioSuscribir(forms.Form):
    email = forms.EmailField(label='Email:', max_length=254, widget=forms.TextInput(attrs={'class':'form-control input-inline', 'placeholder':'Ej: jperezaracena@gmail.com', 'pattern':'[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}', 'required':''}))

class FormularioCongreso(forms.Form):
    nombre = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'class': 'form-control input-inline estilo_form', 'placeholder':'Ej: Juan Perez','required':''}))
    institucion = forms.CharField(max_length=150, label='Institución', widget=forms.TextInput(attrs={'class': 'form-control input-inline estilo_form', 'placeholder':'Ej: Universidad de La Serena'}))
    correo = forms.EmailField(max_length=254, label='Email', widget=forms.TextInput(attrs={'class':'form-control input-inline estilo_form', 'placeholder':'Ej: mail@mail.com', 'pattern':'[A-Z0-9a-z]+([._%+-]*[A-Z0-9a-z]+)?@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}', 'required':''}))
