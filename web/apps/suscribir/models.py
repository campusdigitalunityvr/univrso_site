#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.db import models

class Suscribir(models.Model):
    email = models.EmailField(max_length=254, blank=False, null=False)

class Congreso(models.Model):
    nombre = models.CharField(max_length=150)
    institucion = models.CharField(max_length=150)
    correo = models.EmailField(max_length=254, blank=False, null=False)
