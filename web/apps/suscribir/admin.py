# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Suscribir, Congreso


class SuscribirAdmin(admin.ModelAdmin):
    list_display = ('id','email')

    class Meta:
        verbose_name_plural = "Suscriptores"


class CongresoAdmin(admin.ModelAdmin):
    list_display = ('nombre','institucion','correo')

    class Meta:
        verbose_name_plural = "Inscritos"


admin.site.register(Suscribir, SuscribirAdmin)
admin.site.register(Congreso, CongresoAdmin)
