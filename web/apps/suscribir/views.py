# -*- coding: utf-8 -*-
from django.shortcuts import render
from .models import Suscribir, Congreso
from .forms import FormularioSuscribir, FormularioCongreso
from django.http import HttpResponse, JsonResponse
from django.utils import timezone
from django.conf import settings
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr
from email.utils import formatdate
from email.utils import make_msgid
import json
import datetime
import smtplib
import time



def inscripcion_congreso(request):
    if request.method == "POST":
        form_congreso = FormularioCongreso(request.POST)
        if form_congreso.is_valid():
            nuevo_congreso = Congreso.objects.create(
                nombre = form_congreso.cleaned_data.get('nombre'),
                institucion = form_congreso.cleaned_data.get('institucion'),
                correo = form_congreso.cleaned_data.get('correo')
            )
            nuevo_congreso.save()
            if(enviar_correo(nuevo_congreso)):
                return HttpResponse(json.dumps({"estado": "ok_congreso_email"}), content_type="application/json")
            else:
                return HttpResponse(json.dumps({"estado": "ok_congreso"}), content_type="application/json")
        else:
            return HttpResponse(json.dumps({"estado": "nok_congreso"}),content_type="application/json")
    else:
        return HttpResponse(json.dumps({"estado": "nok_post"}), content_type="application/json")



def enviar_correo(inscripcion):
    try:
        template = """
        <head>
            <link rel="stylesheet" type="text/css" href="http://universo.userena.digital/static/css/custom.css">
            <meta charset="utf-8" />
        </head>
        <body>
            <h3>Estimado(a) """+str(inscripcion.nombre)+""":</h3>
            <p>Usted ha sido registrado(a) al encuentro Astroturístico: Vivamos el Eclipse, a realizarse el día 9 de Abril a las 14:30 en la Biblioteca Regional Gabriela Mistral.</p>
            <br/>
            <p>Consultas al email campusd@userena.cl</p>
            <br/>
             <hr style="color:#E5E5E5">
             <div class="col-footer">
               <img src="http://universo.userena.digital/static/images/logos/congreso_astroturismo.png" alt="congreso" align="middle" height="100px">
               <img src="http://universo.userena.digital/static/images/logos/logos_congreso.jpg" align="middle" height="100px">
             </div>
         </body>
         """
        toaddr  = ""+str(inscripcion.correo)
        if (enviar_email(template,toaddr)):
            return True
        else:
            return False

    except Exception as e:
        print(str(e)) # for just the message
        # print(e.args)
        # print(e)
        return False
    return True



# Función que envía correro dada una plantilla,
def enviar_email(cuerpo_correo, toaddr):
    try:
        username = settings.EMAIL_HOST_USER
        password = settings.EMAIL_HOST_PASSWORD
        fromaddr = "continuidad@userena.digital"

        msg = MIMEMultipart('alternative')

        # Recipientes del correos de producción
        bcc = 'cevillegas@userena.cl'
        toaddrs = [toaddr] + [bcc]

        msg['Subject'] = "Confirmación de Inscripción Encuentro Astroturístico"
        msg['From'] = formataddr((str(Header('Univrso', 'utf-8')), fromaddr))
        msg['To'] = toaddr
        msg['Date'] = formatdate(time.time(), localtime=True)
        msg.add_header('Message-id', make_msgid())
        msg.attach(MIMEText(cuerpo_correo, 'html'))

        # Configuración del servidor de correo
        server = smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT)
        server.ehlo()
        server.starttls()
        server.login(username, password)
        server.sendmail(fromaddr, toaddrs, msg.as_string())
        print("Email enviado")
        server.close()
    except Exception as e:
        print(str(e))  # for just the message print(e.args) print(e)
        return False
    return True


# Formulario para suscribirse a lista de correos Universo
def suscribir_correos(request):
    if request.method == "POST":
        formulario = FormularioSuscribir(request.POST)
        if formulario.is_valid():
            nuevo = Suscribir.objects.create(
                email = formulario.cleaned_data.get('email')
            )
            nuevo.save() # formulario = FormularioSuscribir()
            return HttpResponse(json.dumps({"estado": "ok_lista_correo"}), content_type="application/json")
        else:
            return HttpResponse(json.dumps({"estado": "nok_lista_correo"}), content_type="application/json")
    else:
        return HttpResponse(json.dumps({"estado": "nok_post"}), content_type="application/json")
