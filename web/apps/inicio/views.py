# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from ..noticias.models import Noticia
from ..equipo.models import Equipo, Categorias
from ..suscribir.models import Suscribir, Congreso
from ..suscribir.forms import FormularioSuscribir, FormularioCongreso
from ..moocs.models import Mooc


# Create your views here.
def vista_inicio(request):
    noticias = Noticia.objects.all()[:5]
    categorias = Categorias.objects.all().order_by('prioridad')
    equipos = Equipo.objects.all()
    cientificos = Equipo.objects.filter(categoria__nombre='cientifico').order_by('prioridad')
    administrativos = Equipo.objects.filter(categoria__nombre= 'administrativo').order_by('prioridad')
    tecnicos = Equipo.objects.filter(categoria__nombre= 'tecnico').order_by('prioridad')
    audiovisuales = Equipo.objects.filter(categoria__nombre= 'audiovisual').order_by('prioridad')
    moocs = Mooc.objects.filter(visible=True).order_by('prioridad')

    # Lectura variables de formulario
    form_congreso = FormularioCongreso()
    formulario = FormularioSuscribir()

    return render(request, 'inicio.html', {'form_congreso': form_congreso,'formulario': formulario, 'noticias': noticias, 'categorias': categorias, 'equipos': equipos, 'cientificos': cientificos , 'administrativos': administrativos , 'tecnicos': tecnicos, 'audiovisuales': audiovisuales, 'moocs': moocs})


def vista_desarrollo(request):
    return render(request, 'endesarrollo.html',)

def vista_congreso(request):
    return render(request, 'congreso.html',)

def handler404(request):
    return render(request, '404.html', status=404)

def handler500(request):
    return render(request, '500.html', status=500)
