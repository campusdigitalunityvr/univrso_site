# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Video


class VideoAdmin(admin.ModelAdmin):
    list_display= ('titulo', 'fecha_actual', 'youtube_id', 'image_img', 'activo')
    search_fields = ('titulo', 'fecha_actual')
    ordering = ('-fecha_actual',)


admin.site.register(Video, VideoAdmin)
