# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-04-29 18:47
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('multimedia', '0006_merge_20190429_1546'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='fecha_actual',
            field=models.DateTimeField(default=datetime.datetime(2019, 4, 29, 15, 47, 21, 904887)),
        ),
    ]
