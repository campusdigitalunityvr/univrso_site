# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2023-04-03 19:34
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('multimedia', '0012_auto_20191204_1233'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='fecha_actual',
            field=models.DateTimeField(default=datetime.datetime(2023, 4, 3, 19, 34, 20, 584387)),
        ),
    ]
