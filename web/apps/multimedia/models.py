from django.db import models
from datetime import datetime
from django.core.files.images import get_image_dimensions
from django.core.exceptions import ValidationError


def validate_image(fieldfile_obj):
    filesize = fieldfile_obj.file.size
    w, h = get_image_dimensions(fieldfile_obj)
    megabyte_limit = 1.0
    if filesize > megabyte_limit*2*1024*1024:
        raise ValidationError("El tamano maximo permitido es de %sMB" % str(megabyte_limit))
    if w != 1280 or h != 720:
        raise ValidationError("Las dimensiones de la imagen son de %ix%i pixeles, y estas deben ser de 1280x720px" %(h,w))


# Create your models here.
class Video(models.Model):
    fecha_modificado = models.DateTimeField(auto_now = True)
    fecha_actual = models.DateTimeField(default=datetime.now())
    titulo = models.CharField(max_length=200)
    epigrafe = models.CharField(max_length=200)
    youtube_id = models.CharField(max_length=100)
    imagen = models.ImageField(upload_to='multimedia/', height_field=None, width_field=None, max_length=200, validators=[validate_image], help_text='Tamano maximo de la imagen es de 2Mb y sus dimensiones deben ser de 1280x720px')
    descripcion = models.TextField(max_length=1500)
    activo = models.BooleanField(default=True)

    def __unicode__(self):
        return self.titulo

    class Meta:
        ordering = ["fecha_modificado"]
        verbose_name_plural = "Videos"

    def image_img(self):
        if self.imagen:
            return u'<img width="125px" height="auto" src="%s" />' % self.imagen.url
        else:
            return '(Sin imagen)'
    image_img.short_description = 'Imagen'
    image_img.allow_tags = True
