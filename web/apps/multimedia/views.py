from django.shortcuts import render
from .models import Video

# Create your views here.
def vista_video(request):
    videos = Video.objects.all()
    return render(request, 'multimedia.html', {'videos' :videos})
