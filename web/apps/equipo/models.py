#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models
from django.core.files.images import get_image_dimensions
from django.core.exceptions import ValidationError
from ckeditor.fields import RichTextField


def validate_image(fieldfile_obj):
    filesize = fieldfile_obj.file.size
    w, h = get_image_dimensions(fieldfile_obj)
    megabyte_limit = 2.0
    if filesize > megabyte_limit*2048*1024:
        raise ValidationError("El tamano maximo permitido es de %sMB" % str(megabyte_limit))
    if w != 417 and h != 417:
        raise ValidationError("Las dimensiones de la imagen son de %ix%i pixeles, y estas deben ser de 417x417px" %(h,w))


class Categorias(models.Model):
    nombre = models.CharField(max_length=200, blank=False, null=False)
    nombre_en = models.CharField(max_length=200, blank=False, null=False)
    prioridad = models.PositiveSmallIntegerField(default=1, blank=False, null=False)

    class Meta:
        verbose_name_plural = "Categorias"

    def __str__(self):
        return '%s' % self.nombre

class Equipo(models.Model):
    nombre = models.CharField(max_length=200, blank=False, null=False)
    cargo = models.CharField(max_length=200, blank=False, null=False)
    cargo_en = models.CharField(max_length=200, blank=False, null=False)
    email =  models.EmailField(blank=True, unique=True)
    imagen = models.ImageField(upload_to='equipo/', height_field=None, width_field=None, max_length=100, validators=[validate_image], help_text='Tamano maximo de la imagen es de 2Mb y sus dimensiones deben ser de 417x417px')
    categoria = models.ForeignKey(Categorias, blank=True, null=True, related_name='Categorias')
    prioridad = models.PositiveSmallIntegerField(default=1, blank=False, null=False)

# models.ForeignKey(Categorias, on_delete=models.CASCADE)
# class Meta:
#     verbose_name_plural = "Equipo"
