# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Equipo, Categorias


class EquipoAdmin(admin.ModelAdmin):
    list_display = ('nombre','cargo', 'categoria')


class CategoriasAdmin(admin.ModelAdmin):
    list_display = ('nombre',)


admin.site.register(Equipo, EquipoAdmin)
admin.site.register(Categorias, CategoriasAdmin)