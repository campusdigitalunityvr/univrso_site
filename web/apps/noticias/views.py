# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from .models import Noticia

# Vista Noticia
def vista_noticia(request, noticia_id):
    ultimas = Noticia.objects.all().order_by('-id')
    n = Noticia.objects.filter(id=noticia_id)
    if n.exists():
        return render(request, 'noticias.html', {'noticia': n, 'ultimas': ultimas})
    return redirect('apps.inicio.views.vista_inicio')


def vista_todas(request):
    noticias = Noticia.objects.all().order_by('-id')
    return render(request, 'todaslasnoticias.html', {'noticias': noticias})
