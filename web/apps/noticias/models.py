#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models
from datetime import datetime
from django.core.files.images import get_image_dimensions
from django.core.exceptions import ValidationError
from ckeditor.fields import RichTextField


def validate_image(fieldfile_obj):
    filesize = fieldfile_obj.file.size
    w, h = get_image_dimensions(fieldfile_obj)
    megabyte_limit = 2.0
    if filesize > megabyte_limit*2048*1024:
        raise ValidationError("El tamano maximo permitido es de %sMB" % str(megabyte_limit))
    if w != 1280 and h != 720:
        raise ValidationError("Las dimensiones de la imagen son de %ix%i pixeles, y estas deben ser de 1280x720px" %(h,w))


class Noticia(models.Model):
    titulo = models.CharField(max_length=200, blank=False, null=False)
    titulo_en = models.CharField(max_length=200, blank=False, null=False)
    cuerpo = RichTextField(max_length=10000, config_name='awesome_ckeditor')
    cuerpo_en = RichTextField(max_length=10000, config_name='awesome_ckeditor')
    imagen = models.ImageField(upload_to='noticias/', height_field=None, width_field=None, max_length=100, validators=[validate_image], help_text='Tamano maximo de la imagen es de 2Mb y sus dimensiones deben ser de 1280x720px')
    autor = models.CharField(max_length=200,blank=False, null=False)
    pie_imagen = models.CharField(max_length=300, blank=True, null=True)
    pie_imagen_en = models.CharField(max_length=300, blank=True, null=True)
    fecha_creacion = models.DateTimeField()

    class Meta:
        ordering = ['-fecha_creacion']
        verbose_name_plural = "noticias"

    def __unicode__(self):
        return self.titulo;

    def image_foto(self):
        if self.imagen:
            return u'<img width="125px" height="auto" src="%s" />' % self.imagen.url
        else:
            return '(Sin imagen)'
    image_foto.short_description = 'Imagen'
    image_foto.allow_tags = True
