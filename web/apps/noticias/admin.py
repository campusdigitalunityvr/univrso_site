# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Noticia


class NoticiaAdmin(admin.ModelAdmin):
    list_display = ('fecha_creacion','titulo', 'image_foto',)
    search_fields = ('fecha_creacion','titulo')
    list_filter = ('fecha_creacion',)
    ordering = ('-fecha_creacion',)


admin.site.register(Noticia, NoticiaAdmin)
