# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Mooc


class MoocAdmin(admin.ModelAdmin):
    list_display = ('id','titulo', 'imagen','prioridad','visible',)
    ordering = ('prioridad',)


admin.site.register(Mooc, MoocAdmin)
