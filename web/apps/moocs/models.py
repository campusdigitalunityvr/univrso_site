from django.db import models
from django.core.files.images import get_image_dimensions
from django.core.exceptions import ValidationError
from ckeditor.fields import RichTextField

def validate_image(fieldfile_obj):
    filesize = fieldfile_obj.file.size
    w, h = get_image_dimensions(fieldfile_obj)
    megabyte_limit = 2.0
    if filesize > megabyte_limit*2048*1024:
        raise ValidationError("El tamano maximo permitido es de %sMB" % str(megabyte_limit))
    if w != 600 and h != 600:
        raise ValidationError("Las dimensiones de la imagen son de %ix%i pixeles, y estas deben ser de 600x600px" %(h,w))


# Create your models here.
class Mooc(models.Model):
    titulo = models.CharField(max_length=200, blank=False, null=False)
    titulo_en = models.CharField(max_length=200, blank=False, null=False)
    cuerpo = RichTextField(max_length=2000, config_name='awesome_ckeditor')
    cuerpo_en = RichTextField(max_length=2000, config_name='awesome_ckeditor')
    imagen = models.ImageField(upload_to='mooc/', height_field=None, width_field=None, max_length=100, validators=[validate_image], help_text='Tamano maximo de la imagen es de 2Mb y sus dimensiones deben ser de 600x600px')
    enlace = models.CharField(max_length=70, blank=True, null=True)
    prioridad = models.PositiveSmallIntegerField(default=1, blank=False, null=False)
    visible = models.BooleanField(default=True)
