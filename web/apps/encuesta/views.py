# -*- coding: utf-8 -*-
from django.shortcuts import render
from .models import Encuesta
from .forms import FormularioEncuesta
from django.http import HttpResponse
from django.views.decorators.clickjacking import xframe_options_exempt

# Visualización Encuesta para MOOC
#@xframe_options_exempt
def vista_encuesta(request):
    form_encuesta = FormularioEncuesta()
    if request.method == "POST":
        form_encuesta = FormularioEncuesta(request.POST)
        regiones = request.POST['regiones'].encode('utf8')
        comunas = request.POST['comunas'].encode('utf8')

        if form_encuesta.is_valid():
            informo = form_encuesta.cleaned_data.get('informo').encode('utf8')
            if informo == 'otro':
                informo = "otro: "+request.POST['otro_medio']

            nueva_encuesta = Encuesta.objects.create(
                email = form_encuesta.cleaned_data.get('email'),
                informo = informo,
                region = regiones,
                comuna = comunas,
                mejoraria = form_encuesta.cleaned_data.get('mejoraria'),
                realizaria = form_encuesta.cleaned_data.get('realizar')
            )
            nueva_encuesta.save()
            form_encuesta = FormularioEncuesta()
            return render(request, 'encuesta.html', {'form_encuesta': form_encuesta, 'procesado':'procesado'})
        else:
            return render(request, 'encuesta.html', {'form_encuesta': form_encuesta})
            # print("Email: "+form_encuesta.cleaned_data.get('email').encode('utf8'))
            # print("informo: "+form_encuesta.cleaned_data.get('informo').encode('utf8'))
            # print("mejoraria: "+form_encuesta.cleaned_data.get('mejoraria').encode('utf8'))
            # print("realizar: "+form_encuesta.cleaned_data.get('realizar').encode('utf8'))
    else:
        return render(request, 'encuesta.html', {'form_encuesta': form_encuesta})
