#!/usr/bin/python
# -*- coding: utf-8 -*-
from django import forms
from .models import Encuesta

# Formulario utilizado en encuestas para los MOOC
class FormularioEncuesta(forms.Form):
    CHOICEINFORMO = [('redes', 'Redes Sociales'),
                 ('correo', 'Correo electrónico'),
                 ('charlas', 'Charlas de Astronomía'),
                 ('otro', 'Otro')]

    OECD = [ ('ninguno', ''),
             ('Matematicas', 'Matemáticas'),
             ('Ciencias de la Informacion y Computacion', 'Ciencias de la Información y Computación'),
             ('Ciencias Fisicas', 'Ciencias Físicas'),
             ('Ciencias Quimicas', 'Ciencias Químicas'),
             ('Ciencias de la Tierra y del Medio Ambiente', 'Ciencias de la Tierra y del Medio Ambiente'),
             ('Ciencias Biologicas', 'Ciencias Biológicas'),
             ('Ingenieria Civil', 'Ingeniería Civil'),
             ('Ingenieria Electrica, Ingenieria Electronica, Informatica', 'Ingeniería Eléctrica, Ingeniería Electrónica, Informática'),
             ('Ingenieria Mecanica', 'Ingeniería Mecánica'),
             ('Ingenieria Quimica', 'Ingeniería Química'),
             ('Ingenieria de los Materiales', 'Ingeniería de los Materiales'),
             ('Ingenieria Medica', 'Ingeniería Médica'),
             ('Ingenieria Ambiental', 'Ingeniería Ambiental'),
             ('Biotecnologia Ambiental', 'Biotecnología Ambiental'),
             ('Biotecnologia Industrial', 'Biotecnología Industrial'),
             ('Nanotecnologia', 'Nanotecnología'),
             ('Medicina Basica', 'Medicina Básica'),
             ('Medicina Clinica', 'Medicina Clínica'),
             ('Ciencias de la Salud', 'Ciencias de la Salud'),
             ('Biotecnologia Medica', 'Biotecnología Médica'),
             ('Agricultura, Silvicultura, Pesca', 'Agricultura, Silvicultura, Pesca'),
             ('Ciencia Animal y Lecheria', 'Ciencia Animal y Lechería'),
             ('Ciencias Veterinarias Biotecnologia Agricola', 'Ciencias Veterinarias Biotecnología Agrícola'),
             ('Psicologia', 'Psicología'),
             ('Economia y Negocios', 'Economía y Negocios'),
             ('Ciencias de la Educacion', 'Ciencias de la Educación'),
             ('Sociologia', 'Sociología'),
             ('Antropologia', 'Antropología'),
             ('Derecho', 'Derecho'),
             ('Ciencias Politicas', 'Ciencias Políticas'),
             ('Geografia Economica y Social', 'Geografía Económica y Social'),
             ('Comunicacion y Medios', 'Comunicación y Medios'),
             ('Historia y Arqueologia', 'Historia y Arqueología'),
             ('Lenguage y Literatura', 'Lenguage y Literatura'),
             ('Filosofia, Etica y Religion', 'Filosofía, Etica y Religión'),
             ('Arte', 'Arte (Artes, Historia del Arte, Artes Escénicas, Música)')
           ]

    email = forms.EmailField(max_length=254, label='Email (*)', widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Ingrese su Email', 'pattern':'[A-Z0-9a-z]+([._%+-]*[A-Z0-9a-z]+)?@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}', 'required':''}))
    informo = forms.ChoiceField(label='¿A través de qué medios se informó del curso? (*)', choices=CHOICEINFORMO, initial='redes', widget=forms.RadioSelect(attrs={'class':'form-check'}))
    mejoraria = forms.CharField(max_length=200,label='¿Qué nos recomendaría para mejorar nuestro curso? (*)', widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Indique su recomendación'}))
    realizar = forms.ChoiceField(label='¿Qué MOOC le gustaría realizar con nosotros? (*)', choices=OECD,  initial='ninguno', widget=forms.Select(attrs={'class':'form-control','id': 'realizar'}))
