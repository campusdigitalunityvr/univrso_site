# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import Encuesta


class EncuestaAdmin(admin.ModelAdmin):
   list_display = ('email','region', 'comuna')


admin.site.register(Encuesta, EncuestaAdmin)
