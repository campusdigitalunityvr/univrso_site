#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.db import models

# Modelo de la Encuesta para MOOC
class Encuesta(models.Model):
    email = models.EmailField(max_length=100, blank=False, null=False)
    informo = models.CharField(max_length=250, default='')
    mejoraria = models.CharField(max_length=200, default='')
    realizaria = models.CharField(max_length=250, default='')
    region = models.CharField(max_length=200, default='')
    comuna = models.CharField(max_length=200, default='')
