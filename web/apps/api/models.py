# -*- coding: utf-8 -*-

from django.db import models
import datetime


class Datos(models.Model):
    version = models.CharField(unique=True, max_length=100, blank=False, null=False)
    fecha = models.DateField(default=datetime.date.today)
    archivo = models.FileField(blank=False, upload_to='api')

    class Meta:
        ordering = ['-fecha']
        verbose_name_plural = "datos"