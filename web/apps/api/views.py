# -*- coding: utf-8 -*-

from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework import viewsets
from .serializers import DatosSerializer
from .models import Datos


class DatosViewSet(viewsets.ModelViewSet):
    queryset = Datos.objects.all()
    serializer_class = DatosSerializer
    http_method_names = ['get']
