# -*- coding: utf-8 -*-

from django.contrib import admin
from ..api.models import Datos

class DatosAdmin(admin.ModelAdmin):
    list_display = ('version','fecha','archivo',)
    search_fields = ('fecha',)
    list_filter = ('fecha',)
    ordering = ('-fecha',)

admin.site.register(Datos, DatosAdmin)
